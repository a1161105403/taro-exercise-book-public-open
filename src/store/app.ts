import { InitialStateType } from './typings';

const initialState: InitialStateType = {

};
export default {
  state: initialState,
  reducers: {
    save(state, payload) {
      return { ...state, ...payload };
    },
  },
  effects: (dispatch) => ({
    saveData(payload) {
      dispatch.app.save(payload);
    },
  }),
};
