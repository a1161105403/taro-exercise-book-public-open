import { init, Models, RematchDispatch, RematchRootState } from '@rematch/core';
import createLoadingPlugin from '@rematch/loading';

import app from './app';
import user from './models/user';
const loadingPlugin = createLoadingPlugin();

interface RootModel extends Models<RootModel> {
  app: typeof app;
  user: typeof user;
}

export type RootState = RematchRootState<RootModel>;
export type Dispatch = RematchDispatch<RootModel>;

const config: { models: RootModel; plugins: any[] } = {
  models: {
    app,
    user,
  },
  plugins: [loadingPlugin],
};

const store = init(config);

export default store;
