import { createModel } from "@rematch/core";
import { InitialState } from "./typings";
const initialState: InitialState = {};

const user = createModel()({
  state: initialState,
  reducers: {
    save(state, payload) {
      return { ...state, ...payload };
    },
  },
  effects: (dispatch) => ({}),
});
export default user;
