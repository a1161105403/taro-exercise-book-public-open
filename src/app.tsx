import { Provider } from "react-redux";
import "./app.less";

import store from "./store/index";
function App({ children }): any {
  return <Provider store={store}>{children}</Provider>;
}

export default App;
