// 获取当前时间
export const getCurrentDateTime = () => {
  const date = new Date();
  const year = date.getFullYear();
  const month = String(date.getMonth() + 1).padStart(2, "0"); // 月份是从0开始计数的，因此要加1
  const day = String(date.getDate()).padStart(2, "0");
  const hours = String(date.getHours()).padStart(2, "0");
  const minutes = String(date.getMinutes()).padStart(2, "0");
  const seconds = String(date.getSeconds()).padStart(2, "0");
  return `${year}-${month}-${day} ${hours}:${minutes}:${seconds}`;
};
// 将日期时间转为数组格式
export const convertTimeStringToArray = (
  timeString: string
): number[] | null => {
  const date = new Date(timeString);
  if (Number.isNaN(date.getTime())) {
    // 如果解析失败，返回 null
    return null;
  }
  const year = date.getFullYear();
  const month = date.getMonth() + 1; // 月份是从0开始计数的，所以要加1
  const day = date.getDate();
  const hours = date.getHours();
  const minutes = date.getMinutes();
  const seconds = date.getSeconds();
  return [year, month, day, hours, minutes, seconds];
};
// 获取年
export const getYear = (): string[] => {
  const currentYear = 2020;
  const arrs: string[] = [];
  for (let i = currentYear; i <= currentYear + 50; i++) {
    arrs.push(i?.toString() + "年");
  }
  return arrs;
};
// 获取月份
export const getMouth = (): string[] => {
  let months: string[] = [];
  for (let i = 1; i <= 12; i++) {
    let element: string = i.toString();
    if (i < 10) {
      element = "0" + i;
    }
    months.push(element + "月");
  }
  return months; //返回月份数组
};
// 获取日
export const getDay = (yearSelected?: string, selectedMonths?: string) => {
  if (!yearSelected || !selectedMonths) {
    let days: string[] = [];
    for (let i = 1; i <= 30; i++) {
      let element: string = i.toString();
      if (i < 10) {
        element = "0" + i;
      }
      days.push(element + "日");
    }
    return days; //返回月份数组
  }
  const isLeapYear = (year) =>
    (year % 4 === 0 && year % 100 !== 0) || year % 400 === 0;

  const daysInMonth = {
    "01月": 31,
    "02月": isLeapYear(extractDateTime(yearSelected)) ? 29 : 28,
    "03月": 31,
    "04月": 30,
    "05月": 31,
    "06月": 30,
    "07月": 31,
    "08月": 31,
    "09月": 30,
    "10月": 31,
    "11月": 30,
    "12月": 31,
  };

  const maxDays = daysInMonth[selectedMonths as string] || 0;
  const days = Array.from({ length: maxDays }, (_, index) => {
    const day = index + 1;
    return day < 10 ? `0${day}日` : `${day}日`;
  });

  return days;
};
// 获取小时/分/秒
export const getHourMinuteSecond = (
  start: number,
  end: number,
  type: string
) => {
  let hours: string[] = [];
  for (let i = start; i < end; i++) {
    let element: string = i.toString();
    if (i < 10) {
      element = "0" + i;
    }
    hours.push(element + type);
  }
  return hours;
};
// 初始化下标
export const getInitDateIndexs = () => {
  const date = new Date();
  const currentYear = date.getFullYear();
  const currentMonth = date.getMonth() + 1; // 月份是从0开始计数的，因此要加1
  const currentDay = date.getDate();
  const currentHour = date.getHours();
  const currentMinute = date.getMinutes();
  const currentSecond = date.getSeconds();
  const years = getYear();
  const months = getMouth();
  const days = getDay(String(currentYear), String(currentMonth));
  const hours = getHourMinuteSecond(0, 24, "时");
  const minutes = getHourMinuteSecond(0, 60, "分");
  const seconds = getHourMinuteSecond(0, 60, "秒");
  const yearIndex = getCurrentIndex(years, currentYear, "年");
  const monthIndex = getCurrentIndex(months, currentMonth, "月");
  const dayIndex = getCurrentIndex(days, currentDay, "日");
  const hourIndex = getCurrentIndex(hours, currentHour, "时");
  const minuteIndex = getCurrentIndex(minutes, currentMinute, "分");
  const secondIndex = getCurrentIndex(seconds, currentSecond, "秒");
  const indexs = [
    yearIndex,
    monthIndex,
    dayIndex,
    hourIndex,
    minuteIndex,
    secondIndex,
  ];
  const dateTime = `${currentYear}-${currentMonth}-${currentDay} ${currentHour}:${currentMinute}:${currentSecond}`;
  return {
    indexs,
    dateTime,
  };
};
// 找到当前下标
export const getCurrentIndex = (
  source: string[],
  target: string | number,
  type?: string
) => {
  let copy = target;
  if (Number(copy) < 10) {
    copy = "0" + copy;
  }
  if (type) {
    copy += type;
  }
  return source.findIndex((item: string) => item === copy);
};
// 去掉0开头的 如01 02
export const removeLeadingZeros = (input: string) => {
  // 使用正则表达式匹配以 0 开头的数字，但排除 10, 20 等情况
  const result = input.replace(/^0(\d+)$/, "$1");
  return result;
};
// 日期格式化
export const extractDateTime = (text: string): string => {
  return text.replace(/[\u4e00-\u9fa5]/g, "");
};
