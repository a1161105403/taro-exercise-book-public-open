import React from "react";

export type PropsType = {
  value?: string;
  name?: string;
  children?: React.ReactElement;
  onchange?: (p: string) => void;
};
