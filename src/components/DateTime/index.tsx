import { Picker } from '@tarojs/components'
import React, { useEffect, useState } from 'react'
import { convertTimeStringToArray, getDay, getHourMinuteSecond, getMouth, getYear, getInitDateIndexs, getCurrentIndex, removeLeadingZeros, extractDateTime } from './utils';
import { PropsType } from './typings';
import styles from './index.module.less'
const DateTime: React.FC<PropsType> = (props) => {
  const { value, children, onchange, name } = props
  const [range, setRange] = useState<string[][]>([]);
  const [valueIndexs, setValueIndexs] = useState<number[]>([]);
  const [time, setTime] = useState<string>('');
  useEffect(() => {
    const years = getYear()
    const months = getMouth()
    const days = getDay()
    const hours = getHourMinuteSecond(0, 24, '时')
    const minutes = getHourMinuteSecond(0, 60, '分')
    const seconds = getHourMinuteSecond(0, 60, '秒')

    const origin = [years, months, days, hours, minutes, seconds]
    setRange(origin)
    // 回显赋值
    if (value) {
      const current: number[] = convertTimeStringToArray(value) as number[]
      const yearIndex = getCurrentIndex(years, current[0], '年');
      const monthIndex = getCurrentIndex(months, current[1], '月');
      const dayIndex = getCurrentIndex(days, current[2], '日');
      const hourIndex = getCurrentIndex(hours, current[3], '时');
      const minuteIndex = getCurrentIndex(minutes, current[4], '分');
      const secondIndex = getCurrentIndex(seconds, current[5], '秒');
      setValueIndexs([yearIndex, monthIndex, dayIndex, hourIndex, minuteIndex, secondIndex])
      setTime(value)
      return
    }
    // 使用当前时间
    const { indexs, dateTime } = getInitDateIndexs()
    setTime(dateTime)
    setValueIndexs(indexs)

  }, [value])

  const changeYearOrMouth = (column: number, value: number) => {

    let currentYear: string = '', currentMonth: string = ''
    // 年
    if (column === 0) {
      currentYear = range[0][value]
      currentMonth = range[1][valueIndexs[1]]
      valueIndexs[0] = value

    }
    // 月
    if (column === 1) {
      currentYear = range[0][valueIndexs[0]]
      currentMonth = range[1][value]
      // 下标重新赋值
      valueIndexs[1] = value
    }
    // 根据年月计算日范围
    const days = getDay(currentYear, removeLeadingZeros(currentMonth))
    // 日 重新赋值
    range[2] = days
    setRange([...range])
    // 查找当前选择日下标是否存在
    const currentDayIndex = getCurrentIndex(days, range[2][valueIndexs[2]])
    // 找不到需要重新赋值
    if (currentDayIndex === -1) {
      valueIndexs[2] = days.length - 1
    }
    setValueIndexs([...valueIndexs])
  }
  const onColumnChange = ({ detail: { column, value } }: any) => {
    // 选择年/月 需要重新计算日
    if ([0, 1].includes(column)) {
      changeYearOrMouth(column, value)
      return
    }
    valueIndexs[column] = value
  }
  const change: any = ({ target: { value } }: any) => {
    const currentYear = range[0][value[0]]
    const currentMonth = range[1][value[1]]
    const currentDay = range[2][value[2]]
    const currentHour = range[3][value[3]]
    const currentMinute = range[4][value[4]]
    const currentSecond = range[5][value[5]]
    const currrentDateTime = `${currentYear}-${currentMonth}-${currentDay} ${currentHour}:${currentMinute}:${currentSecond}`;
    const dateTime = extractDateTime(currrentDateTime)
    onchange?.(dateTime)
    setTime(dateTime)

  }
  return (
    <Picker
      name={name}
      className={styles.picker}
      mode="multiSelector"
      onColumnChange={onColumnChange}
      value={valueIndexs}
      range={range}
      onChange={change}
      onCancel={() => {
        console.log('取消');
      }}
    >
      {children ?? time}
    </Picker>
  )
}

export default DateTime
