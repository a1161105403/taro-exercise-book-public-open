import React, { useState, useRef, useEffect } from "react";
import Taro, { CanvasContext } from "@tarojs/taro";
import { Canvas, Button, Image, View } from "@tarojs/components";
import styles from "./index.module.less";
const SignatureCanvas = ({ onSignatureSave }) => {
  const [hasDraw, setHasDraw] = useState(false);
  const [ctx, setCtx] = useState<CanvasContext | any>(null);
  const init = () => {
    const context = Taro.createCanvasContext("canvasId");
    setCtx(context);
    context.setStrokeStyle("#000000");
    context.setLineWidth(3);
  };
  useEffect(() => {
    init();
  }, []);
  const touchStart = function (e) {
    console.log("touchstart1", e);
    console.log("touchstart1", ctx);
    if (!ctx) return;
    const context: any = ctx.moveTo(e.touches[0].x, e.touches[0].y);
    setCtx(context);
    setHasDraw(true); // 要签字了
  };
  const touchMove = function (e) {
    let x = e.touches[0].x;
    let y = e.touches[0].y;
    ctx.setLineWidth(3);
    ctx.lineTo(x, y);
    ctx.stroke();
    ctx.setLineCap("round");
    ctx.draw(true);
    ctx.moveTo(x, y);
  };
  return (
    <Canvas
      className={styles.canvas}
      canvasId="canvasId"
      onTouchStart={touchStart}
      onTouchMove={touchMove}
      style={{ height: 375, width: 375, backgroundColor: "red" }}
    />
  );
};

export default SignatureCanvas;
