import React from "react";

export type ToDayPickerType = {
  toDay?: boolean;
  onChange?: (d: string) => void;
  children: React.ReactElement;
  defalutValue?: string;
};
