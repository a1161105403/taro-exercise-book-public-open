export const fisrtYearStr: string = "至今";
export const lastYearStr: string = "1990以前";
const currentYear: number = new Date().getFullYear();
export const getYear = (): string[] => {
  const arrs: string[] = [];
  for (let i = currentYear; i >= 1990; i--) {
    arrs.push(i?.toString());
  }
  arrs.push(lastYearStr);
  return arrs;
};
export const getMouth = (year: number = currentYear): string[] => {
  const currentMonth = new Date().getMonth();
  const arrs: string[] = [];
  const currentMouth: number = currentYear == year ? currentMonth : 12;
  for (let i = currentMouth; i > 0; i--) {
    arrs.push(String(i));
  }
  return arrs;
};
