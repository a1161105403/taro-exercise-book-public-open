import Taro from "@tarojs/taro";
import { View } from "@tarojs/components";
import Node from "./node"; // Assuming you have a Node component

const Index = ({
  selectable,
  containerStyle,
  lazyLoad,
  loadingImg,
  errorImg,
  showImgMenu,
  nodes,
}) => {
  return (
    <View
      className={`_root ${selectable ? "_select" : ""}`}
      style={containerStyle}
    >
      {!nodes[0] && <slot />}
      <Node
        id="_root"
        childs={nodes}
        opts={[lazyLoad, loadingImg, errorImg, showImgMenu, selectable]}
        catchadd="_add"
      />
    </View>
  );
};
export default Index;
