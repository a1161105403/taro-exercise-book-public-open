import { Block, Button, Text, View } from "@tarojs/components";
import WxParseTemplate from "./WxParseTemplate";
import WxParseAudio from "./wxParseAudio";
import WxParseImg from "./WxParseImg";
import WxParseVideo from "./WxParseVideo";
import WxParseTable from "./wxParseTable";
const Index = (props: any) => {
  const { node, onNavigate, parseSelect } = props;
  const elementTag = () => {
    /* <!--判断是否是标签节点--> */
    if (node.node !== "element") return;
    if (node.tag == "Button") {
      return (
        <Button type="default" size="mini">
          {node?.nodes?.map((item: any, index: number) => (
            <Block key={index}>
              <WxParseTemplate node={item} />
            </Block>
          ))}
        </Button>
      );
    }
    /* <!--li类型--> */
    if (node.tag == "li") {
      return (
        <Block>
          <View className={node?.classStr} style={node.styleStr}>
            {node?.nodes?.map((item: any, index: number) => (
              <Block key={index}>
                <WxParseTemplate node={item} />
              </Block>
            ))}
          </View>
        </Block>
      );
    }

    /* <!--video类型--> */
    if (node.tag == "video") {
      return (
        <Block>
          <WxParseVideo node={node} />
        </Block>
      );
    }

    /* <!--audio类型--> */
    if (node.tag == "audio") {
      return (
        <Block>
          <WxParseAudio node={node} />
        </Block>
      );
    }
    /* <!--img类型--> */
    if (node.tag == "img") {
      return (
        <Block>
          <WxParseImg node={node} />
        </Block>
      );
    }

    /* <!--a类型--> */
    if (node.tag == "a") {
      return (
        <Block>
          <View
            onClick={wxParseATap}
            className={node?.classStr}
            style={node.styleStr}
            data-href={node.attr.href}
          >
            {node?.nodes?.map((item: any, index: number) => (
              <Block key={index}>
                <WxParseTemplate node={item} />
              </Block>
            ))}
          </View>
        </Block>
      );
    }
    /* <!--table类型--> */
    if (node.tag == "table") {
      return (
        <WxParseTable parseSelect={parseSelect} node={node} />
        // <Block>
        //   <View className={`table ${node?.classStr}`} style={node.styleStr}>
        //     {node?.nodes?.map((item: any, index: number) => (
        //       <Block key={index}>
        //         <WxParseTemplate node={item} />
        //       </Block>
        //     ))}
        //   </View>
        // </Block>
      );
    }
    /* <!--br类型--> */
    if (node.tag == "br") {
      return (
        <Block>
          <Text decode>
            <br />
          </Text>
        </Block>
      );
    }
    // <!--其他标签-->
    return (
      <View className={node?.classStr} style={node.styleStr}>
        {node?.nodes?.map((item: any, index: number) => (
          <Block key={index}>
            <WxParseTemplate node={item} />
          </Block>
        ))}
      </View>
    );
  };
  const wxParseATap = (e) => {
    const { href } = e.currentTarget.dataset;
    if (!href) return;
    onNavigate?.(href, e);
  };
  return (
    <>
      <view>
        <Block>{elementTag()}</Block>
        {/* <!--判断是否是文本节点--> */}
        {node.node == "text" && <Block>{node?.text}</Block>}
      </view>
    </>
  );
};

export default Index;
