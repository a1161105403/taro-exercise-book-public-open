const WxParseVideo = ({ node }) => {
  return (
    <div
      className={node.tag === "video" ? node.classStr : ""}
      style={node.styleStr}
    >
      {node.tag === "video" && (
        <video
          className={`video-video ${node.tag === "video" ? node.classStr : ""}`}
          src={node.attr.src}
        ></video>
      )}
    </div>
  );
};

export default WxParseVideo;
