import { Audio } from "@tarojs/components";

const WxParseAudio = (props: any) => {
  const { node = {} } = props;
  return (
    // <!--增加audio标签支持-->
    <Audio
      id={node.attr.id}
      class={node.classStr}
      style={node.styleStr}
      src={node.attr.src}
      loop={node.attr.loop}
      poster={node.attr.poster}
      name={node.attr.name}
      author={node.attr.author}
      controls
    ></Audio>
  );
};

export default WxParseAudio;
