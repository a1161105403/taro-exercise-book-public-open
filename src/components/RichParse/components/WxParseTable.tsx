import React, { useState, useEffect } from "react";
import { RichText } from "@tarojs/components";
import "../index.less";
const WxParseTable = ({ node, parseSelect }) => {
  const [nodes, setNodes] = useState([]);

  useEffect(() => {
    setNodes(loadNode([node]));
  }, [node]);

  const loadNode = (nodesArray) => {
    let obj: any = [];
    for (let children of nodesArray) {
      if (children.node === "element") {
        let t = {
          name: children.tag,
          attrs: {
            className: children.classStr,
            style: children.styleStr,
          },
          children: children.nodes ? loadNode(children.nodes) : [],
        };

        obj.push(t);
      } else if (children.node === "text") {
        obj.push({
          type: "text",
          text: children.text,
        });
      }
    }
    return obj;
  };

  return (
    <div className="tablebox">
      <RichText
        nodes={nodes}
        className={node.classStr}
        style={"userSelect" + parseSelect}
      />
    </div>
  );
};

export default WxParseTable;
