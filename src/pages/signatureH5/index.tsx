import React, { useRef } from 'react'
import ReactDOM from 'react-dom'
import SignatureCanvas from 'react-signature-canvas'
const Index = () => {
  const sigCanvas = useRef()
  return (
    <SignatureCanvas
      penColor="black"
      ref={sigCanvas}
      canvasProps={{ width: 500, height: 750, className: 'sigCanvas' }}
      maxWidth={1.5}
    />
  )
}
export default Index