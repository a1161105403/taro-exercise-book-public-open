import { RichText } from "@tarojs/components";
import React from "react";
import { mockText, str } from "./lib/mock";
// import "./index.less";
import RichParse from "./../../components/RichParse/index";
// import "../../components/UParse/u-parse.css";
const Index = () => {
  const preview = (src, e) => {
    // do something
  };
  const navigate = (href, e) => {
    // do something
  };
  return (
    <div>
      {/* <RichText nodes={mockText} /> */}
      <RichParse content={mockText} preview={preview} navigate={navigate} />
    </div>
  );
};
export default Index;
