import React, { useState } from "react";
import DateTime from '@/components/DateTime/index';
import { getCurrentDateTime } from "@/components/DateTime/utils";

const Index = () => {
  const [currentTime] = useState<string>(getCurrentDateTime());
  const [value, setValue] = useState<string>(currentTime);
  return < >
    <DateTime name="time" value={currentTime} />
    <br />
    <DateTime name="time" value={currentTime} onchange={(v: string) => setValue(v)}>
      <span style={{ color: 'red' }}>{value}</span>
    </DateTime>
  </>
};

export default Index;
