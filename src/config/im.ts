// 如果您已集成 v2.x 的 SDK，想升级到 V3 并且想尽可能地少改动项目代码，可以继续沿用 TIM
// import TIM from '@tencentcloud/chat';
import TencentCloudChat from "@tencentcloud/chat";
import TIMUploadPlugin from "tim-upload-plugin";
import TIMProfanityFilterPlugin from "tim-profanity-filter-plugin";

let options = {
  SDKAppID: 1600009281, // 接入时需要将0替换为您的即时通信 IM 应用的 SDKAppID
};
// 创建 SDK 实例，`TIM.create()`方法对于同一个 `SDKAppID` 只会返回同一份实例
let chat = TencentCloudChat.create(options); // SDK 实例通常用 chat 表示

// chat.setLogLevel(0); // 普通级别，日志量较多，接入时建议使用
chat.setLogLevel(1); // release 级别，SDK 输出关键信息，生产环境时建议使用

// 注册腾讯云即时通信 IM 上传插件
chat.registerPlugin({ "tim-upload-plugin": TIMUploadPlugin });

// 注册腾讯云即时通信 IM 本地审核插件
chat.registerPlugin({
  "tim-profanity-filter-plugin": TIMProfanityFilterPlugin,
});
TencentCloudChat.create(options);
// SDK 进入 ready 状态时触发，接入侧监听此事件，然后可调用 SDK 发送消息等 API，使用 SDK 的各项功能。
const onSdkReady = function (event) {
  let message = chat.createTextMessage({
    to: "user1",
    conversationType: TencentCloudChat.TYPES.CONV_C2C,
    payload: { text: "Hello world!" },
  });
  chat.sendMessage(message);
};
chat.on(TencentCloudChat.EVENT.SDK_READY, onSdkReady);
