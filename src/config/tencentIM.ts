import TencentCloudChat from "@tencentcloud/chat";
import type { ChatSDK } from "@tencentcloud/chat";
import TIMUploadPlugin from "tim-upload-plugin";
import TIMProfanityFilterPlugin from "tim-profanity-filter-plugin";

class TencentIM {
  private chat: ChatSDK;
  private SDKAppID: number; //即时通信 IM 应用的 SDKAppID
  private userID: string; //用户 ID。
  private userSig: string; //用户登录即时通信 IM 的密码，其本质是对 UserID 等信息加密后得到的密文。具体生成方法请参见 生成 UserSig https://cloud.tencent.com/document/product/269/32688
  constructor(options) {
    this.init(options);
  }
  // 初始化
  private init(options: any) {
    const { SDKAppID, userID, userSig } = options;
    this.SDKAppID = SDKAppID;
    this.userID = userID;
    this.userSig = userSig;

    this.chat = TencentCloudChat.create({
      SDKAppID,
    });
    // 接收消息
    this.chat.on(TencentCloudChat.EVENT.MESSAGE_RECEIVED, this.receiveMessage);

    // 登录
    this.login(userID, userSig);

    // 注册腾讯云即时通信 IM 上传插件
    this.chat.registerPlugin({ "tim-upload-plugin": TIMUploadPlugin });

    // 注册腾讯云即时通信 IM 本地审核插件
    this.chat.registerPlugin({
      "tim-profanity-filter-plugin": TIMProfanityFilterPlugin,
    });
  }
  // 发送消息
  sendMessage(to: string, text: string) {
    const message = this.chat.createTextMessage({
      to,
      conversationType: TencentCloudChat.TYPES.CONV_C2C,
      payload: { text },
    });
    this.chat.sendMessage(message);
  }
  // 登录
  private async login(userID: string, userSig: string) {
    try {
      const imResponse: any = this.chat.login({
        userID,
        userSig,
      });
      if (imResponse.data.repeatLogin === true) {
        // 标识账号已登录，本次登录操作为重复登录。
        console.log(imResponse.data.errorInfo);
      }
    } catch (error) {
      console.warn("login error:", error); // 登录失败的相关信息
    }
  }
  // 登出
  async logout() {
    try {
      const imResponse: any = await this.chat.logout();
      console.log(imResponse.data); // 登出成功
    } catch (error) {
      console.warn("logout error:", error);
    }
  }
  // 获取历史消息
  async getHistoryMessages(
    conversationID: string,
    nextReqMessageID: string | undefined
  ) {
    //nextReqMessageID 用于分页续拉的消息 ID。第一次拉取时不要传入 nextReqMessageID，续拉时填入上次调用 getMessageList 接口返回的该字段的值。
    let params: any = {
      conversationID,
    };
    if (nextReqMessageID) {
      params = { ...params, nextReqMessageID };
    }
    try {
      const imResponse = await this.chat.getMessageList(params);
      const messageList = imResponse.data.messageList; // 消息列表。
      const nextReqMessageID = imResponse.data.nextReqMessageID; // 用于续拉，分页续拉时需传入该字段。
      const isCompleted = imResponse.data.isCompleted; // 表示是否已经拉完所有消息。
    } catch (error) {
      console.log("获取历史消息:", error);
    }
  }
  // 接收消息
  receiveMessage(event: any) {
    // event.data - 存储 Message 对象的数组 - [Message]
    console.log("event", event);

    const messageList = event.data;
    messageList.forEach((message) => {
      if (message.type === TencentCloudChat.TYPES.MSG_TEXT) {
        // 文本消息 - https://web.sdk.qcloud.com/im/doc/v3/zh-cn/Message.html#.TextPayload
      } else if (message.type === TencentCloudChat.TYPES.MSG_IMAGE) {
        // 图片消息 - https://web.sdk.qcloud.com/im/doc/v3/zh-cn/Message.html#.ImagePayload
      } else if (message.type === TencentCloudChat.TYPES.MSG_AUDIO) {
        // 音频消息 - https://web.sdk.qcloud.com/im/doc/v3/zh-cn/Message.html#.AudioPayload
      } else if (message.type === TencentCloudChat.TYPES.MSG_VIDEO) {
        // 视频消息 - https://web.sdk.qcloud.com/im/doc/v3/zh-cn/Message.html#.VideoPayload
      } else if (message.type === TencentCloudChat.TYPES.MSG_FILE) {
        // 文件消息 - https://web.sdk.qcloud.com/im/doc/v3/zh-cn/Message.html#.FilePayload
      } else if (message.type === TencentCloudChat.TYPES.MSG_CUSTOM) {
        // 自定义消息 - https://web.sdk.qcloud.com/im/doc/v3/zh-cn/Message.html#.CustomPayload
      } else if (message.type === TencentCloudChat.TYPES.MSG_MERGER) {
        // 合并消息 - https://web.sdk.qcloud.com/im/doc/v3/zh-cn/Message.html#.MergerPayload
      } else if (message.type === TencentCloudChat.TYPES.MSG_LOCATION) {
        // 地理位置消息 - https://web.sdk.qcloud.com/im/doc/v3/zh-cn/Message.html#.LocationPayload
      } else if (message.type === TencentCloudChat.TYPES.MSG_GRP_TIP) {
        // 群提示消息 - https://web.sdk.qcloud.com/im/doc/v3/zh-cn/Message.html#.GroupTipPayload
      } else if (message.type === TencentCloudChat.TYPES.MSG_GRP_SYS_NOTICE) {
        // 群系统通知 - https://web.sdk.qcloud.com/im/doc/v3/zh-cn/Message.html#.GroupSystemNoticePayload
      }
    });
  }
}

export default TencentIM;
