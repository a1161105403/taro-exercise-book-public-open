const pages = [
  "pages/im/index",
  "pages/signature/index",
  "pages/index/index",
  "pages/richText/index",
  "pages/dateTime/index",
  "pages/signatureH5/index",
];
export default defineAppConfig({
  pages,
});
