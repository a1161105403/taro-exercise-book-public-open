module.exports = {
  extends: ["taro/react"],
  rules: {
    "react/jsx-uses-react": "off",
    "react/react-in-jsx-scope": "off",
    "jsx-quotes": "off",
    "import/newline-after-import": "off",
    "import/first": "off",
    "import/no-commonjs": "off",
    "react-hooks/exhaustive-deps": "off",
  },
};
